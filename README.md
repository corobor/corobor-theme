# corobor-theme

This project contains differents themes for corobor applications.

Use pertinent theme function of the framework used.

## Angular

### Install

```
npm install https://bitbucket.org:corobor/corobor-theme.git --save
```

In styles.scss
```scss
@import '~corobor-theme/src/angular-material.scss';
@font-face {
  font-family: 'Roboto';
  src: url('~corobor-theme/src/Roboto-Regular.ttf');
}
```

In angular.json, in section :  
**projects.PROJECT_NAME.architect.build.option.assets**

```json
"assets": [
  {
    "glob": "**/*.ico",
    "input": "./node_modules/corobor-theme/src",
    "output": "/"
  },
  "src/assets"
],
```

